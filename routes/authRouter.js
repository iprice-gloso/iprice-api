const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

router.get('/', async (req, res) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        console.log(decoded.userId);
        if( !decoded.userId ) {
            return res.json({ authenticated: false });
        } else {
            return res.json({ authenticated: true });
        }

    } catch (err) {
        console.log(err)
        return res.json({ authenticated: false });
    }
});

module.exports = router;