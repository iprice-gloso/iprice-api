const express = require('express');
const router = express.Router();
const Blog = require('../models/blog');
const checkAuth = require('../auth/checkauth');

//Get all blog posts
router.get('/', async (req, res) => {
    try {
        const posts = await Blog.find();
        res.send(posts);
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})

//Get single blog by id
router.get('/:id', getBlog, (req, res) => {
    res.json(res.blog);
})


//Create new blog
router.post('/', checkAuth, async (req, res) => {
    const blogPost = new Blog({
        title: req.body.title,
        author: req.body.author,
        postBody: req.body.postBody,
        postDate: req.body.postDate
    })
    try {
        const newPost = await blogPost.save();
        res.status(201).json(newPost);
    } catch (err) {
        res.status(400).json({ message: err.message })
    }
})

//Update blog

//Delete

async function getBlog(req, res, next) {
    let blog;
    try {
        blog = await Blog.findById(req.params.id);
        if(blog == null) {
            return res.status(404).json({ message: "Cannot find blog post" })
        }
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }
    res.blog = blog;
    next();
}

module.exports = router;