POST http://localhost:4000/blog
Content-Type: application/json

{
    "title": "Test with auth",
    "postBody": "Fuckin smokin bby",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQGlzYWFjcHJpY2UubWUiLCJ1c2VySWQiOiI1ZTE4ZWY1YWNhZDQ1MTBiMTg1MGY4OGYiLCJpYXQiOjE1Nzg3OTE3OTIsImV4cCI6MTU3ODc5NTM5Mn0.W0J_0Z84_fho7DqQYG2sMDt_1Frui8-s2tynSO1Za8U"
}

###

GET http://localhost:4000/blog

###

GET http://localhost:4000/blog/5e1619ce7259606a363470d8

###

GET http://localhost:4000/user

###

POST http://localhost:4000/user/register
Content-Type: application/json

{
    "username": "JuneBugsz",
    "email": "bugabooze@isaacpricer.me",
    "password": "password123"
}

###

POST http://localhost:4000/user/login
Content-Type: application/json

{
    "username": "Drrtbag",
    "password": "password123"
}

###

DELETE http://localhost:4000/user/5e18f1d3def6d30d7f88d91c

###

GET http://localhost:4000/auth
Content-Type: application/json;
Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQGlzYWFjcHJpY2UubWUiLCJ1c2VySWQiOiI1ZTE4ZWY1YWNhZDQ1MTBiMTg1MGY4OGYiLCJpYXQiOjE1NzkxMTY0MjMsImV4cCI6MTU3OTEyMDAyM30.JIfpLnF5nv3QYCov33pOxNuD5T1Rg9VYHtqNXGYzJGg
