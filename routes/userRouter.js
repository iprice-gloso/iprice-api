const express = require('express');
const router = express.Router();
const User = require('../models/user');
const app = express();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

app.use(express.urlencoded({ extended: false }));

router.get('/', async (req, res) => {
    try {
        const users = await User.find();
        res.send(users);
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})

router.post('/register', async (req, res, next) => {
    const cursorUsername = User.collection.find({ username: req.body.username });
    const cursorEmail = User.collection.find({ email: req.body.email });
    if((await cursorUsername.hasNext())) {
        return res.status(409).json({ message: 'Username already taken' });
    } else if(await cursorEmail.hasNext()) {
        return res.status(409).json({ message: 'Email already exists' })
    }
     else {
        try {
            const hashedPass = await bcrypt.hash(req.body.password, 10);
            const user = new User({
                username: req.body.username,
                email: req.body.email,
                password: hashedPass,
            })
            const newUser = await user.save();
            res.status(201).json(newUser);
        } catch (err) {
            res.status(500).json({ message: err.message })
        }
    }
});

router.post('/login', async (req, res, next) => {
    const cursor = User.collection.find({username: req.body.username}, {username: 1, _id: 1, password: 1});
    if(!(await cursor.hasNext())) {
        return res.status(401).json({ message: 'Cannot find user with that username' });
    }
    const user = await cursor.next();
    try {
    if(await bcrypt.compare(req.body.password, user.password)) {
        const token = jwt.sign({
            email: user.email,
            userId: user._id
        }, process.env.JWT_SECRET, { expiresIn: "1h" })
        return res.status(201).json({
            message: 'User Authenticated',
            token: token
        });
    } else {
        return res.status(400).json({ 
            authenticated: false,
            username: req.body.username,
            password: req.body.password
        })
    }
    } catch (err) {
        return res.status(500).json({ message: err })
    }
});

router.delete('/:id', (req, res, next) => {
    User.remove({ _id: req.params.id })
    .exec()
    .then(result => {
        res.status(200).json({ message: 'User Deleted' })
    })
    .catch(err => {
        res.status(400).json({ message: 'No user with the given id exists, nothing to delete' })
    })
})

module.exports = router;