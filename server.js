require('dotenv').config();
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');

app.use(cors());

app.use(
    bodyParser.urlencoded({
        extended: true
    })
)

app.use(bodyParser.json());

const corsOptions = {
    exposedHeaders: 'Authorization',
  };
  
app.use(cors(corsOptions));

mongoose.connect(process.env.DATABASE_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const db = mongoose.connection;

db.on('error', (error) => console.error(error));
db.once('open', () => console.log("Connected to the database"));

app.use(express.json());

const blogRouter = require('./routes/blogRouter');
app.use('/blog', blogRouter);

const userRouter = require('./routes/userRouter');
app.use('/user', userRouter);

const authRouter = require('./routes/authRouter');
app.use('/auth', authRouter);

app.listen(4000, () => console.log("server started at port 4000"));